package com.gridnine.testing;

import com.gridnine.testing.models.Flight;
import com.gridnine.testing.services.FlightFilter;
import com.gridnine.testing.services.FlightFilterImpl;
import com.gridnine.testing.util.ConsolePrintOut;
import com.gridnine.testing.util.FlightBuilder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Flight> flights = FlightBuilder.createFlights();

        Scanner scanner = new Scanner(System.in);

        ConsolePrintOut.printFlights(flights);

        FlightFilter flightFilter = new FlightFilterImpl();

        while (true) {
            String input = scanner.nextLine();
            if (input.equals("1")) {
                ConsolePrintOut.printFlights(flightFilter.departureUpToTheCurrentTime(flights, LocalDateTime.now()));
            }

            if (input.equals("2")) {
                ConsolePrintOut.printFlights(flightFilter.dateOfArrivalBeforeTheDateOfDeparture(flights));
            }

            if (input.equals("3")) {
                ConsolePrintOut.printFlights(flightFilter.timeSpentOnTheGroundExceedsTwoHours(flights));
            }

            if (input.equals("4")) {
                break;
            }
        }
    }
}
