package com.gridnine.testing.services;

import com.gridnine.testing.models.Flight;
import com.gridnine.testing.models.Segment;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FlightFilterImpl implements FlightFilter {

    @Override
    public List<Flight> departureUpToTheCurrentTime(List<Flight> flights, LocalDateTime currentTime) {
        List<Flight> result = new ArrayList<>(flights);
        for (Flight flight : flights) {
            for (Segment segment : flight.getSegments()) {
                if (segment.getDepartureDate().isBefore(currentTime)) {
                    result.remove(flight);
                }
            }
        }
        return result;
    }

    @Override
    public List<Flight> dateOfArrivalBeforeTheDateOfDeparture(List<Flight> flights) {
        List<Flight> result = new ArrayList<>(flights);
        for (Flight flight : flights) {
            List<Segment> segments = flight.getSegments();
            for (Segment segment : segments) {
                if (segment.getArrivalDate().isBefore(segment.getDepartureDate())) {
                    result.remove(flight);
                }
            }
        }
        return result;
    }

    @Override
    public List<Flight> timeSpentOnTheGroundExceedsTwoHours(List<Flight> flights) {
        List<Flight> result = new ArrayList<>(flights);
        for (Flight flight : flights) {
            List<Segment> segments = flight.getSegments();
            int timeOnTheGround = 0;
            if (segments.size() > 1) {
                for (int i = 0; i < segments.size() - 1; i++) {
                    timeOnTheGround += Math.abs(Duration.between(segments.get(i).getArrivalDate(), segments.get(i + 1).getDepartureDate()).toHours());
                }
                if (timeOnTheGround > 2) {
                    result.remove(flight);
                }
            }
        }
        return result;
    }
}
