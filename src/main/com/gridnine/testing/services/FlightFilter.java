package com.gridnine.testing.services;

import com.gridnine.testing.models.Flight;

import java.time.LocalDateTime;
import java.util.List;

public interface FlightFilter {
    List<Flight> departureUpToTheCurrentTime(List<Flight> flights, LocalDateTime currentTime);

    List<Flight> dateOfArrivalBeforeTheDateOfDeparture(List<Flight> flights);

    List<Flight> timeSpentOnTheGroundExceedsTwoHours(List<Flight> flights);
}
