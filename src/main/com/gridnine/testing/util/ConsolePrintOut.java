package com.gridnine.testing.util;

import com.gridnine.testing.models.Flight;

import java.util.List;

public class ConsolePrintOut {
    public static void printFlights(List<Flight> flights) {
        System.out.println("Список полетов:" + "\n");

        for (Flight flight : flights) {
            System.out.println(flight);
            System.out.println("*****");
        }

        System.out.println("Какие полеты вы хотите исключить?");
        System.out.println("(1) С вылетом до текущего момента времени.");
        System.out.println("(2) Те полеты, в которых имеются сегменты с датой прилета раньше даты вылета.");
        System.out.println("(3) Те полеты, у которых общее время проведенное на земле, превышает два часа.");
        System.out.println("(4) Завершить работу программы.");

    }
}
